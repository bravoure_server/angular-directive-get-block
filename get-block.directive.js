(function () {
    'use strict';

    function getBlockDirective ($compile) {

        return {
            restrict: "EA",
            replace: true,
            scope: {
                module: '=',
                type_list: '=',
                bindScrollTo: '='
            },

            controller: function($scope, $controller) {

                angular.extend(this, $controller('baseController', {
                    $scope: $scope
                }));

                var vm = this;

            },

            compile: function compile(tElement, tAttrs, transclude) {
                return {
                    pre: function preLink(scope, iElement, iAttrs, controller) {

                        // Defines the type. for example - 'block-item'
                        scope.field_type = (typeof iAttrs.fieldType != 'undefined') ? iAttrs.fieldType : '';
                        scope.template = (typeof iAttrs.template != 'undefined') ? ' template="' + iAttrs.template + '"': '';

                        // Defines the type of list - for example 'masonry'
                        scope.field_type_list = (typeof iAttrs.fieldTypeList != 'undefined' && iAttrs.fieldTypeList != '') ? '_' + iAttrs.fieldTypeList : '';

                        scope.showGhost = true;
                        // ghost directive
                        var htm = '<ghosts-cards></ghosts-cards>';
                        var compiled = $compile(htm)(scope);

                        iElement.html(compiled);

                    },
                    post: function postLink(scope, iElement, iAttrs, controller) {
                        //var params = {};

                        if (!angular.isUndefined(scope.module.type)) {

                            var target = scope.module.type.replace(/([A-Z])/g, '_$1');

                            // Removing the title after the "__"
                            if (target.indexOf('__') != -1) {

                                var targetArray = target.split('__');

                                target = targetArray[0];
                            }
                        }

                        var bindScrollTo = (scope.bindScrollTo) ? ' bind-scroll-to="' +scope.bindScrollTo+'"' : '' ;

                        var tag = ((scope.field_type != '') ? scope.field_type + scope.field_type_list : target );
                        var htm = '<' + tag + ' id="' + scope.module.id + '"' + ' module="' + scope.module + '" field_type="'+scope.field_type+'" ' + bindScrollTo + scope.template +'></' + tag + '>';

                        var compiled = $compile(htm)(scope);

                        iElement.html(compiled);

                    }
                };
            },

            link: function (scope, element, attrs) {

            },

            template: '<div></div>'
        }
    }

    getBlockDirective.$inject = ['$compile'];

    angular
        .module('bravoureAngularApp')
        .directive('getBlockDirective', getBlockDirective);

})();
